using Gtk;
using Notify;

public class Header : HeaderBar {
    public Button send_notif;
    public Notify.Notification notify;
    public Header ()
    {
        // Create notification send button
        notify = new Notify.Notification("Test message", "Try", "dialog-information");
        send_notif = new Button.with_label("Send Notification!");
        send_notif.clicked.connect(() =>
        {
            try {
                notify.show();
            }
            catch (Error e){
                error ("Error: %s", e.message);
            }

        });

        pack_end (send_notif);
        show_close_button = true;
        show_all();
    }
}
